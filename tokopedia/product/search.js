'use strict'

import * as cheerio from 'cheerio'
import UserAgent from 'user-agents'
import puppeteer from 'puppeteer-extra'
import stealthPlugin from 'puppeteer-extra-plugin-stealth'

puppeteer.use(stealthPlugin())

const args = ({ encodeQuery = false } = {}) => {
  const args = process.argv.slice(2)
  const parsedArgs = {}

  args.forEach(arg => {
    const [key, value] = arg.split('=')
    parsedArgs[key.substring(2)] = value
  })
  
  const query = (encodeQuery && parsedArgs.q) ? encodeURI(parsedArgs.q) : parsedArgs.q
  const page = parsedArgs.p ?? 1

  return {
    q: query,
    p: page
  }
}
 
(async () => {
  const { q, p } = args({ encodeQuery: true })

  if (!q) {
    throw new Error('Please provide a valid query keyword as a command line argument.')
  }

  const supportingQueries = 'srp_component_id=02.01.00.00&srp_page_id=&srp_page_title=&navsource='
  const url = `https://www.tokopedia.com/search?st=&page=${p}&q=${q}&${supportingQueries}`

  const randomAgent = getRandomUserAgent()

  const browser = await puppeteer.launch({
    headless: 'new',
    args: [
      '--no-sandbox',
      '--disable-setuid-sandbox',
      '--disable-gpu',
      '--disable-gl-drawing-for-tests'
    ]
  })

  const context = await browser.createIncognitoBrowserContext()
  const page = await context.newPage()

  await page.setRequestInterception(true)
  await page.setJavaScriptEnabled(true)
  await page.setUserAgent(randomAgent)
  await page.setViewport({ width: 1920, height: 1080 })

  page.on('request', (request) => {
    if (['image', 'media', 'stylesheet', 'font'].indexOf(request.resourceType()) !== -1) {
      request.abort()
    } else {
      request.continue()
    }
  })

  await page.goto(url, { waitUntil: 'domcontentloaded' })
  await autoScrollToBottom(page)
  await page.waitForSelector('[data-testid="divSRPLazyPagination"]')

  const body = await page.content()
  const $ = cheerio.load(body)
  const productsCard = $('[data-testid="master-product-card"]')

  const results = []

  productsCard.each((_i, el) => {
    // is the product an advertisement?
    const adsIcon = $('[data-testid="divSRPTopadsIcon"]', el).text()
    if (!!adsIcon) return

    // product info
    const name = $('[data-testid="spnSRPProdName"]', el).text()
    const price = $('[data-testid="spnSRPProdPrice"]', el).text()
    const url = $('a[href]', el).attr('href')
    const image = $('img[src]', el).attr('src')
    const rating = $('.prd_rating-average-text', el).text()
    const sold = $('.prd_label-integrity', el).text()

    // seller info
    const sellerName = $('.prd_link-shop-name', el).text()
    const sellerLocation = $('.prd_link-shop-loc', el).text()

    if (
      !name.length ||
      !price.length ||
      !url.length ||
      !image.length ||
      !rating.length ||
      !sold.length
    ) {
      return
    }

    // get slug from url
    const slug = new URL(url).pathname.substring(1)

    results.push({
      name,
      price,
      url,
      image,
      rating,
      sold,
      slug,
      seller: {
        name: sellerName,
        location: sellerLocation
      }
    })
  })

  const response = {
    query: decodeURI(q),
    current_page: p,
    total: results.length,
    results
  }

  console.log(JSON.stringify(response))

  await page.close()
  await browser.close()
})()

function getRandomUserAgent() {  
  const userAgent = new UserAgent({
    deviceCategory: 'desktop'
  })
  
  return userAgent.data.userAgent
}

async function autoScrollToBottom(page) {
  let timer

  await page.evaluate(async () => {
    await new Promise((resolve, _reject) => {
      let totalHeight = 0
      const distance = 1000
      const interval = 100

      timer = setInterval(() => {
        const scrollHeight = document.body.scrollHeight
        window.scrollBy(0, distance)
        totalHeight += distance

        if (totalHeight >= scrollHeight) { resolve() }
      }, interval)
    })
  })

  clearInterval(timer)
}
