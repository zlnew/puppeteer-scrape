'use strict'

import * as cheerio from 'cheerio'
import UserAgent from 'user-agents'
import puppeteer from 'puppeteer-extra'
import stealthPlugin from 'puppeteer-extra-plugin-stealth'

puppeteer.use(stealthPlugin())

const args = () => {
  const args = process.argv.slice(2)
  const parsedArgs = {}

  args.forEach(arg => {
    const [key, value] = arg.split('=')
    parsedArgs[key.substring(2)] = value
  })
  
  const slug = parsedArgs.s

  return {
    s: slug
  }
}
 
(async () => {
  const result = {
    media: {},
    content: {},
    variants: [],
    detail: {}
  }

  const { s } = args()

  if (!s) {
    throw new Error('Please provide a valid slug as a command line argument.')
  }

  const supportingQueries = 'extParam=ivf%3Dfalse'
  const url = `https://www.tokopedia.com/${s}?${supportingQueries}`

  const randomAgent = getRandomUserAgent()

  const browser = await puppeteer.launch({
    headless: 'new',
    args: [
      '--no-sandbox',
      '--disable-setuid-sandbox',
      '--disable-gpu',
      '--disable-gl-drawing-for-tests'
    ]
  })

  const context = await browser.createIncognitoBrowserContext()
  const page = await context.newPage()

  await page.setRequestInterception(true)
  await page.setJavaScriptEnabled(true)
  await page.setUserAgent(randomAgent)
  await page.setViewport({ width: 1920, height: 1080 })

  page.on('request', (request) => {
    if (['image', 'media', 'stylesheet', 'font'].indexOf(request.resourceType()) !== -1) {
      request.abort()
    } else {
      request.continue()
    }
  })

  await page.goto(url, { waitUntil: 'domcontentloaded' })
  await page.waitForSelector('[data-testid="lblPDPDetailProductRatingCounter"]')
  await page.waitForSelector('[data-testid="lblPDPDetailProductPrice"]')

  // click see delivery button
  await page.waitForSelector('[data-testid="btnShowShipmentModal"]')
    .then(async (el) => {
      await el.click()
      await page.waitForSelector('[data-testid="lblPDPCourierInfo"]')
      await el.dispose()
    })
  
  // cheerio load 
  const body = await page.content()
  const $ = cheerio.load(body)

  // set weight
  const weight = $('[data-testid="lblPDPCourierInfo"]')
    .children('.weight-value')
    .text()

  result.detail.weight = weight

  const $media = $('#pdp_comp-product_media')
  const $content = $('#pdp_comp-product_content')
  const $variants = $('#pdp_comp-new_variant_options')
  const $detail = $('#pdp_comp-product_detail')

  $media.each((_i, el) => {
    const mainImage = $('[data-testid="PDPMainImage"]', el).attr('src')
    const images = []

    result.media = {
      main_image: mainImage,
      images
    }
  })

  $content.each((_i, el) => {
    const name = $('[data-testid="lblPDPDetailProductName"]', el).text()
    const sold = $('[data-testid="lblPDPDetailProductSoldCounter"]', el).text()
    const rating = $('[data-testid="lblPDPDetailProductRatingCounter"]', el).text()
    const price = $('[data-testid="lblPDPDetailProductPrice"]', el).text()

    result.content = {
      name,
      price,
      rating,
      sold
    }
  })

  $variants.each((_i, el) => {
    const $selector = $('.css-1b2d3hk', el).children('.css-1b2d3hk')
    const variants = []

    $selector.each((iS, elS) => {
      const name = $(`[data-testid="pdpVariantTitle#${iS}"]`, elS).text()
      const $items = $('.css-1y1bj62', elS)
      const options = []

      $items.each(async (_iI, elI) => {
        const label = $('button', elI).text()

        options.push({
          label,
          value: label
        })
      })

      variants.push({
        name,
        options
      })
    })

    result.variants = variants
  })

  $detail.each((_i, el) => {
    const description = $('[data-testid="lblPDPDescriptionProduk"]', el).html()

    result.detail = {
      ...result.detail,
      description
    }
  })

  const response = {
    slug: s,
    result
  }

  console.log(JSON.stringify(response))

  await page.close()
  await browser.close()
})()

function getRandomUserAgent() {  
  const userAgent = new UserAgent({
    deviceCategory: 'desktop'
  })
  
  return userAgent.data.userAgent
}
